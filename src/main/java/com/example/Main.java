/*
 * Copyright 2002-2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServlet;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@EnableAutoConfiguration
public class Main extends HttpServlet {

    static Connection c;
    static Statement stat;
    static List<String> reserved;
    static {
        reserved = Arrays.asList(new String[]{"a", "abort", "abs", "absent", "absolute", "access", "according", "action", "ada", "add", "admin", "after", "aggregate", "all", "allocate", "also", "alter", "always", "analyse", "analyze", "and", "any", "are", "array", "array_agg", "array_max_cardinality", "as", "asc", "asensitive", "assertion", "assignment", "asymmetric", "at", "atomic", "attach", "attribute", "attributes", "authorization", "avg", "backward", "base64", "before", "begin", "begin_frame", "begin_partition", "bernoulli", "between", "bigint", "binary", "bit", "bit_length", "blob", "blocked", "bom", "boolean", "both", "breadth", "by", "c", "cache", "call", "called", "cardinality", "cascade", "cascaded", "case", "cast", "catalog", "catalog_name", "ceil", "ceiling", "chain", "char", "character", "characteristics", "characters", "character_length", "character_set_catalog", "character_set_name", "character_set_schema", "char_length", "check", "checkpoint", "class", "class_origin", "clob", "close", "cluster", "coalesce", "cobol", "collate", "collation", "collation_catalog", "collation_name", "collation_schema", "collect", "column", "columns", "column_name", "command_function", "command_function_code", "comment", "comments", "commit", "committed", "concurrently", "condition", "condition_number", "configuration", "conflict", "connect", "connection", "connection_name", "constraint", "constraints", "constraint_catalog", "constraint_name", "constraint_schema", "constructor", "contains", "content", "continue", "control", "conversion", "convert", "copy", "corr", "corresponding", "cost", "count", "covar_pop", "covar_samp", "create", "cross", "csv", "cube", "cume_dist", "current", "current_catalog", "current_date", "current_default_transform_group", "current_path", "current_role", "current_row", "current_schema", "current_time", "current_timestamp", "current_transform_group_for_type", "current_user", "cursor", "cursor_name", "cycle", "data", "database", "datalink", "date", "datetime_interval_code", "datetime_interval_precision", "day", "db", "deallocate", "dec", "decimal", "declare", "default", "defaults", "deferrable", "deferred", "defined", "definer", "degree", "delete", "delimiter", "delimiters", "dense_rank", "depends", "depth", "deref", "derived", "desc", "describe", "descriptor", "detach", "deterministic", "diagnostics", "dictionary", "disable", "discard", "disconnect", "dispatch", "distinct", "dlnewcopy", "dlpreviouscopy", "dlurlcomplete", "dlurlcompleteonly", "dlurlcompletewrite", "dlurlpath", "dlurlpathonly", "dlurlpathwrite", "dlurlscheme", "dlurlserver", "dlvalue", "do", "document", "domain", "double", "drop", "dynamic", "dynamic_function", "dynamic_function_code", "each", "element", "else", "empty", "enable", "encoding", "encrypted", "end", "end-exec", "end_frame", "end_partition", "enforced", "enum", "equals", "escape", "event", "every", "except", "exception", "exclude", "excluding", "exclusive", "exec", "execute", "exists", "exp", "explain", "expression", "extension", "external", "extract", "false", "family", "fetch", "file", "filter", "final", "first", "first_value", "flag", "float", "floor", "following", "for", "force", "foreign", "fortran", "forward", "found", "frame_row", "free", "freeze", "from", "fs", "full", "function", "functions", "fusion", "g", "general", "generated", "get", "global", "go", "goto", "grant", "granted", "greatest", "group", "grouping", "groups", "handler", "having", "header", "hex", "hierarchy", "hold", "hour", "id", "identity", "if", "ignore", "ilike", "immediate", "immediately", "immutable", "implementation", "implicit", "import", "in", "include", "including", "increment", "indent", "index", "indexes", "indicator", "inherit", "inherits", "initially", "inline", "inner", "inout", "input", "insensitive", "insert", "instance", "instantiable", "instead", "int", "integer", "integrity", "intersect", "intersection", "interval", "into", "invoker", "is", "isnull", "isolation", "join", "k", "key", "key_member", "key_type", "label", "lag", "language", "large", "last", "last_value", "lateral", "lead", "leading", "leakproof", "least", "left", "length", "level", "library", "like", "like_regex", "limit", "link", "listen", "ln", "load", "local", "localtime", "localtimestamp", "location", "locator", "lock", "locked", "logged", "lower", "m", "map", "mapping", "match", "matched", "materialized", "max", "maxvalue", "max_cardinality", "member", "merge", "message_length", "message_octet_length", "message_text", "method", "min", "minute", "minvalue", "mod", "mode", "modifies", "module", "month", "more", "move", "multiset", "mumps", "name", "names", "namespace", "national", "natural", "nchar", "nclob", "nesting", "new", "next", "nfc", "nfd", "nfkc", "nfkd", "nil", "no", "none", "normalize", "normalized", "not", "nothing", "notify", "notnull", "nowait", "nth_value", "ntile", "null", "nullable", "nullif", "nulls", "number", "numeric", "object", "occurrences_regex", "octets", "octet_length", "of", "off", "offset", "oids", "old", "on", "only", "open", "operator", "option", "options", "or", "order", "ordering", "ordinality", "others", "out", "outer", "output", "over", "overlaps", "overlay", "overriding", "owned", "owner", "p", "pad", "parallel", "parameter", "parameter_mode", "parameter_name", "parameter_ordinal_position", "parameter_specific_catalog", "parameter_specific_name", "parameter_specific_schema", "parser", "partial", "partition", "pascal", "passing", "passthrough", "password", "path", "percent", "percentile_cont", "percentile_disc", "percent_rank", "period", "permission", "placing", "plans", "pli", "policy", "portion", "position", "position_regex", "power", "precedes", "preceding", "precision", "prepare", "prepared", "preserve", "primary", "prior", "privileges", "procedural", "procedure", "procedures", "program", "public", "publication", "quote", "range", "rank", "read", "reads", "real", "reassign", "recheck", "recovery", "recursive", "ref", "references", "referencing", "refresh", "regr_avgx", "regr_avgy", "regr_count", "regr_intercept", "regr_r2", "regr_slope", "regr_sxx", "regr_sxy", "regr_syy", "reindex", "relative", "release", "rename", "repeatable", "replace", "replica", "requiring", "reset", "respect", "restart", "restore", "restrict", "result", "return", "returned_cardinality", "returned_length", "returned_octet_length", "returned_sqlstate", "returning", "returns", "revoke", "right", "role", "rollback", "rollup", "routine", "routines", "routine_catalog", "routine_name", "routine_schema", "row", "rows", "row_count", "row_number", "rule", "savepoint", "scale", "schema", "schemas", "schema_name", "scope", "scope_catalog", "scope_name", "scope_schema", "scroll", "search", "second", "section", "security", "select", "selective", "self", "sensitive", "sequence", "sequences", "serializable", "server", "server_name", "session", "session_user", "set", "setof", "sets", "share", "show", "similar", "simple", "size", "skip", "smallint", "snapshot", "some", "source", "space", "specific", "specifictype", "specific_name", "sql", "sqlcode", "sqlerror", "sqlexception", "sqlstate", "sqlwarning", "sqrt", "stable", "standalone", "start", "state", "statement", "static", "statistics", "stddev_pop", "stddev_samp", "stdin", "stdout", "storage", "strict", "strip", "structure", "style", "subclass_origin", "submultiset", "subscription", "substring", "substring_regex", "succeeds", "sum", "symmetric", "sysid", "system", "system_time", "system_user", "t", "table", "tables", "tablesample", "tablespace", "table_name", "temp", "template", "temporary", "text", "then", "ties", "time", "timestamp", "timezone_hour", "timezone_minute", "to", "token", "top_level_count", "trailing", "transaction", "transactions_committed", "transactions_rolled_back", "transaction_active", "transform", "transforms", "translate", "translate_regex", "translation", "treat", "trigger", "trigger_catalog", "trigger_name", "trigger_schema", "trim", "trim_array", "true", "truncate", "trusted", "type", "types", "uescape", "unbounded", "uncommitted", "under", "unencrypted", "union", "unique", "unknown", "unlink", "unlisten", "unlogged", "unnamed", "unnest", "until", "untyped", "update", "upper", "uri", "usage", "user", "user_defined_type_catalog", "user_defined_type_code", "user_defined_type_name", "user_defined_type_schema", "using", "vacuum", "valid", "validate", "validator", "value", "values", "value_of", "varbinary", "varchar", "variadic", "varying", "var_pop", "var_samp", "verbose", "version", "versioning", "view", "views", "volatile", "when", "whenever", "where", "whitespace", "width_bucket", "window", "with", "within", "without", "work", "wrapper", "write", "xml", "xmlagg", "xmlattributes", "xmlbinary", "xmlcast", "xmlcomment", "xmlconcat", "xmldeclaration", "xmldocument", "xmlelement", "xmlexists", "xmlforest", "xmliterate", "xmlnamespaces", "xmlparse", "xmlpi", "xmlquery", "xmlroot", "xmlschema", "xmlserialize", "xmltable", "xmltext", "xmlvalidate", "year", "yes", "zone"});
    }

    public static void main(String[] args) throws Exception {

        Class.forName("org.postgresql.Driver");
        c = DriverManager
            .getConnection("jdbc:postgresql://ec2-54-235-247-209.compute-1.amazonaws.com:5432/d8lm34ruobs2uu","ioiobwfxilzfgq","82dba1a2ad1968524c17f382a067e4af10c83f89db3b47df998abbbbd4fa02bf");
        stat = c.createStatement();
        SpringApplication.run(Main.class, args);
    }

    @RequestMapping("/")
    String index(@RequestParam Map<String, String> params, @RequestParam(value = "fileToUpload", required = false) MultipartFile[] files) {
        try {
            JSONObject ans = new JSONObject();
            ResultSet r;
            switch (params.getOrDefault("Type","")) {
                case "CreateTables":
                    stat.execute("create table if not exists Users(id Integer not null, login Varchar Unique, pass Varchar not null);");
                    stat.execute("create table if not exists Devices(owner Varchar not null,name Varchar not null,device Varchar not null, regdate Integer not null);");
                    ans.put("code", 0);
                    return ans.toJSONString();
                case "Login":
                    //Для обращения заполните Login, Pass
                    //code 0 - ошибок нет, code 1 - неверное имя или пароль, code 2 - ошибка приложения, code 3 - пустые поля
                    if (params.containsKey("Login") && params.containsKey("Pass")) {
                        r = stat.executeQuery("SELECT id FROM Users WHERE login='" + params.get("Login") + "' and pass='" + params.get("Pass") + "';");
                        if (r.next()) {
                            ans.put("code", 0);
                            ans.put("id", r.getInt("id"));
                            return ans.toJSONString();
                        } else {
                            ans.put("code", 1);
                            return ans.toJSONString();
                        }
                    } else {
                        ans.put("code", 3);
                        return ans.toJSONString();
                    }
                case "Reg":
                    //Для обращения заполните Login, Pass
                    //code 0 - ошибок нет, code 1 - неверное имя или пароль, code 2 - ошибка приложения, code 3 - пустые поля
                    if (params.containsKey("Login") && params.containsKey("Pass")) {
                        r = stat.executeQuery("SELECT table_name FROM information_schema.tables where table_name='" + params.get("Login") + "';");
                        if (!params.get("Login").matches("\\w+") || reserved.contains(params.get("Login").toLowerCase()) || r.next()){
                            ans.put("code", 1);
                            return ans.toJSONString();
                        }
                        r = stat.executeQuery("SELECT * FROM Users WHERE LOWER(login)=LOWER('" + params.get("Login") + "');");
                        if (!r.next()) {
                            long time = System.currentTimeMillis() / 1000;
                            stat.executeUpdate("INSERT INTO Users(id,login,pass) VALUES(" + time + ",'" + params.get("Login") + "','" + params.get("Pass") + "');");
                            ans.put("code", 0);
                            ans.put("id", time);
                            return ans.toJSONString();
                        } else {
                            ans.put("code", 1);
                            return ans.toJSONString();
                        }
                    } else {
                        ans.put("code", 3);
                        return ans.toJSONString();
                    }
                case "AddDevice":
                    //Для обращения заполните Id, Login, Name, DeviceType(Phone/PC)
                    //code 0 - ошибок нет, code 1 - неверные данные, code 2 - ошибка приложения, code 3 - пустые поля
                    r = stat.executeQuery("SELECT * FROM Users WHERE id=" + params.get("Id") + " and login='" + params.get("Login") + "';");
                    if (r.next()) {
                        r = stat.executeQuery("SELECT * FROM Devices WHERE name='" + params.get("Name") + "' and owner='" + params.get("Login") + "';");
                        boolean exists = r.next();
                        if(!params.get("Login").matches("\\w+")){
                            ans.put("code", 1);
                            return ans.toJSONString();
                        }
                        if (!exists && params.containsKey("Name") && ((params.get("DeviceType").equals("Phone") || params.get("DeviceType").equals("PC")))) {
                            long time = System.currentTimeMillis() / 1000;
                            stat.executeUpdate("INSERT INTO Devices(owner,name,device,regdate) VALUES('" + params.get("Login") + "','" + params.get("Name") + "','" + params.get("DeviceType") + "', " + time + ");");
                            ans.put("code", 0);
                            ans.put("regdate", time);
                            return ans.toJSONString();
                        } else if (!params.containsKey("Name") || !params.containsKey("DeviceType")) {
                            ans.put("code", 3);
                            return ans.toJSONString();
                        } else if ((!params.get("DeviceType").equals("Phone") && !params.get("DeviceType").equals("PC")) || exists) {
                            ans.put("code", 1);
                            return ans.toJSONString();
                        } else {
                            ans.put("code", 2);
                            return ans.toJSONString();
                        }
                    } else {
                        ans.put("code", 2);
                        return ans.toJSONString();
                    }
                case "Check":
                    //Для обращения заполните Login, Id, Name, DeviceType(Phone/PC), RegDate
                    //code 0 - ошибок нет, code 1 - неверные данные, code 2 - ошибка приложения
                    //result 1 - устройство зарегистрировано, result 2 - устройство не зарегистрировано
                    r = stat.executeQuery("SELECT * FROM Users WHERE login='" + params.get("Login") + "' and id='" + params.get("Id") + "';");
                    if (r.next()) {
                        r = stat.executeQuery("SELECT * FROM Devices WHERE device='" + params.get("DeviceType") + "' and name='" + params.get("Name") + "' and owner='" + params.get("Login") + "' and regdate=" + params.get("RegDate") + ";");
                        if (r.next()) {
                            ans.put("code", 0);
                            ans.put("result", 1);
                        } else {
                            ans.put("code", 0);
                            ans.put("result", 2);
                        }
                    } else {
                        ans.put("code", 1);
                    }
                    return ans.toJSONString();
                case "ShowDevices":
                    //Для обращения заполните Login, Id, MyName, RegDate
                    //code 0 - ошибок нет, code 1 - неверные данные, code 2 - ошибка приложения, code 4 - устройство не зарегистрировано
                    r = stat.executeQuery("SELECT * FROM Users WHERE login='" + params.get("Login") + "' and id='" + params.get("Id") + "';");
                    if (!r.next()) {
                        ans.put("code", 1);
                        return ans.toJSONString();
                    }
                    r = stat.executeQuery("SELECT * FROM Devices WHERE owner='" + params.get("Login") + "' and name='" + params.get("MyName") + "' and regdate=" + params.get("RegDate") + ";");
                    if (!r.next()) {
                        ans.put("code", 4);
                        return ans.toJSONString();
                    }
                    ans.put("code", 0);
                    r = stat.executeQuery("SELECT name FROM Devices WHERE device='Phone' and owner='" + params.get("Login") + "';");
                    JSONArray phones = new JSONArray();
                    while (r.next()) {
                        phones.add(r.getString("name"));
                    }
                    ans.put("Phones", phones);
                    r = stat.executeQuery("SELECT name FROM Devices WHERE device='PC' and owner='" + params.get("Login") + "';");

                    JSONArray pcs = new JSONArray();
                    while (r.next()) {
                        pcs.add(r.getString("name"));
                    }
                    ans.put("PCs", pcs);
                    return ans.toJSONString();
                case "RemoveDevice":
                    //Для обращения заполните Login, Id, Name, MyName, RegDate
                    //code 0 - ошибок нет, code 1 - неверные данные, code 2 - ошибка приложения, code 4 - устройство не зарегистрировано
                    r = stat.executeQuery("SELECT * FROM Users WHERE login='" + params.get("Login") + "' and id='" + params.get("Id") + "';");
                    if (!r.next()) {
                        ans.put("code", 1);
                        return ans.toJSONString();
                    }
                    r = stat.executeQuery("SELECT * FROM Devices WHERE owner='" + params.get("Login") + "' and name='" + params.get("MyName") + "' and regdate=" + params.get("RegDate") + ";");
                    if (!r.next()) {
                        ans.put("code", 4);
                        return ans.toJSONString();
                    }
                    stat.execute("DELETE FROM Devices * WHERE name='" + params.get("Name") + "' and owner='" + params.get("Login") + "';");
                    ans.put("code", 0);
                    return ans.toJSONString();
                case "SendMessage":
                    //Для обращения заполните Login, Id, MyName, Msg, RegDate
                    //code 0 - ошибок нет, code 1 - неверные данные, code 2 - ошибка приложения, code 4 - устройство не зарегистрировано
                    r = stat.executeQuery("SELECT * FROM Users WHERE login='" + params.get("Login") + "' and id='" + params.get("Id") + "';");
                    if (!r.next()) {
                        ans.put("code", 1);
                        return ans.toJSONString();
                    }
                    r = stat.executeQuery("SELECT * FROM Devices WHERE owner='" + params.get("Login") + "' and name='" + params.get("MyName") + "' and regdate=" + params.get("RegDate") + ";");
                    if (!r.next()) {
                        ans.put("code", 4);
                        return ans.toJSONString();
                    }
                    stat.execute("create table if not exists " + params.get("Login") + "(date integer, msg varchar, sender varchar, type varchar, filebody bytea);");
                    long time = System.currentTimeMillis() / 1000 - 1;
                    do {
                        time++;
                        r = stat.executeQuery("SELECT * FROM " + params.get("Login") + " where date=" + time + " and msg='" + params.get("Msg") + "';");
                    } while (r.next());
                    stat.execute("INSERT INTO " + params.get("Login") + "(date, msg, sender, type, filebody) VALUES (" + time + ", '" + params.get("Msg") + "', '" + params.get("MyName") + "', 'Text', '');");
                    ans.put("code", 0);
                    ans.put("time", time);
                    return ans.toJSONString();
                case "UploadFile":
                    //Для обращения заполните Login, Id, MyName, RegDate
                    //code 0 - ошибок нет, code 1 - неверные данные, code 2 - ошибка приложения, code 3 - файл не передан, code 4 - устройство не зарегистрировано
                    r = stat.executeQuery("SELECT * FROM Users WHERE login='" + params.get("Login") + "' and id='" + params.get("Id") + "';");
                    if (!r.next()) {
                        ans.put("code", 1);
                        return ans.toJSONString();
                    }
                    r = stat.executeQuery("SELECT * FROM Devices WHERE owner='" + params.get("Login") + "' and name='" + params.get("MyName") + "' and regdate=" + params.get("RegDate") + ";");
                    if (!r.next()) {
                        ans.put("code", 4);
                        return ans.toJSONString();
                    }
                    stat.execute("create table if not exists " + params.get("Login") + "(date integer, msg varchar, sender varchar, type varchar, filebody bytea);");
                    if (files.length == 0) {
                        ans.put("code", 3);
                        return ans.toJSONString();
                    } else {
                        time = System.currentTimeMillis() / 1000;
                        byte[] f = files[0].getBytes();
                        Object[] f2 = new Object[f.length];
                        for(int i = 0; i < f.length; i++)
                            f2[i] = f[i];
                        stat.execute("INSERT INTO " + params.get("Login") + "(date, msg, sender, type, filebody) VALUES (" + time + ", '" + files[0].getResource().getFilename() + "', '" + params.get("MyName") + "', 'File', E'\\\\x" + String.format(repeat("%02x", f2.length), f2) + "'::bytea);");
                        ans.put("code", 0);
                        ans.put("time", time);
                        return ans.toJSONString();
                    }
                case "DownloadFile":
                    //Для обращения заполните Login, Id, MyName, FileName, Date, RegDate
                    //code 0 - ошибок нет, code 1 - неверные данные, code 2 - ошибка приложения, code 3.1 - папка не существует, code 4 - устройство не зарегистрировано
                    r = stat.executeQuery("SELECT * FROM Users WHERE login='"+params.get("Login")+"' and id='"+params.get("Id")+"';");
                    if (!r.next()){
                        ans.put("code",1);
                        return ans.toJSONString();
                    }
                    r = stat.executeQuery("SELECT * FROM Devices WHERE owner='"+params.get("Login")+"' and name='"+params.get("MyName")+"' and regdate="+params.get("RegDate")+";");
                    if (!r.next()){
                        ans.put("code",4);
                        return ans.toJSONString();
                    }
                    r = stat.executeQuery("SELECT * FROM "+params.get("Login")+" where msg='"+params.get("FileName")+"' and date="+params.get("Date")+";");
                    if(r.next()) {
                        ans.put("code", 0);
                        ans.put("filebody", r.getString("FileBody"));
                    }
                    else
                        ans.put("code", 1);
                    return ans.toJSONString();
                case "GetMessages":
                    //Для обращения заполните Login, Id, MyName, RegDate, Count
                    //code 0 - ошибок нет, code 1 - неверные данные, code 2 - ошибка приложения, code 4 - устройство не зарегистрировано
                    r = stat.executeQuery("SELECT * FROM Users WHERE login='" + params.get("Login") + "' and id='" + params.get("Id") + "';");
                    if (!r.next()) {
                        ans.put("code", 1);
                        return ans.toJSONString();
                    }
                    r = stat.executeQuery("SELECT * FROM Devices WHERE owner='" + params.get("Login") + "' and name='" + params.get("MyName") + "' and regdate=" + params.get("RegDate") + ";");
                    if (!r.next()) {
                        ans.put("code", 4);
                        return ans.toJSONString();
                    }
                    r = stat.executeQuery("SELECT * FROM " + params.get("Login") + " ORDER BY date DESC LIMIT " + (Integer.parseInt(params.get("Count"))+1) + ";");
                    ans.put("code", 0);
                    JSONArray messages = new JSONArray();
                    for (int i = 0; i < Integer.parseInt(params.get("Count")) && r.next(); i++) {
                        JSONObject msg = new JSONObject();
                        msg.put("date", r.getInt("date"));
                        msg.put("msg", r.getString("msg"));
                        msg.put("sender", r.getString("sender"));
                        msg.put("type", r.getString("type"));
                        messages.add(msg);
                    }
                    ans.put("hasnext",r.next());
                    ans.put("messages", messages);
                    return ans.toJSONString();
                case "DelMessage":
                    //Для обращения заполните Login, Id, MyName, Msg, Date, RegDate
                    //code 0 - ошибок нет, code 1 - неверные данные, code 2 - ошибка приложения, code 4 - устройство не зарегистрировано
                    r = stat.executeQuery("SELECT * FROM Users WHERE login='" + params.get("Login") + "' and id='" + params.get("Id") + "';");
                    if (!r.next()) {
                        ans.put("code", 1);
                        return ans.toJSONString();
                    }
                    r = stat.executeQuery("SELECT * FROM Devices WHERE owner='" + params.get("Login") + "' and name='" + params.get("MyName") + "' and regdate=" + params.get("RegDate") + ";");
                    if (!r.next()) {
                        ans.put("code", 4);
                        return ans.toJSONString();
                    }
                    stat.execute("DELETE FROM " + params.get("Login") + " * where date=" + params.get("Date") + " and msg='" + params.get("Msg") + "';");
                    ans.put("code", 0);
                    return ans.toJSONString();
                default:
                    ans.put("code", 5);
                    return ans.toJSONString();
            }
        }catch (Exception e){
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            return sw.toString();

        }
    }

    private static String repeat(String s, int a){
        StringBuilder result = new StringBuilder();
        for(int i = 0; i < a; i++){
            result.append(s);
        }
        return result.toString();
    }
}
